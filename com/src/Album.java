package com.songAgas;

import java.util.ArrayList;

public class Album {
    private ArrayList<Song> listSong;
    private String albumName;

    public Album(String albumName) {
        ArrayList<Song> songInAlbumList = new ArrayList<>();
        this.listSong = songInAlbumList;
        this.albumName = albumName;
    }


    public ArrayList<Song> getListSong() {
        return listSong;
    }

    public String getAlbumName() {
        return albumName;
    }

    @Override
    public String toString() {
        return "Album{" +
                "listSong=" + listSong +
                ", albumName='" + albumName + '\'' +
                '}';
    }


  private int findingSong(String songTitle) {
        int index = -1;
        for (int i = 0; listSong.size() > i; i++) {
            if (listSong.get(i).getSongTitle().equals(songTitle)) {
                index = i;
                // System.out.println(index + " Is current index for find Song");
            }
        }
        return index;
    }


    public void addSongInit(String songTitle, double duration) {
        int songLocation = findingSong(songTitle);
        if (songLocation < 0) {
            Song addThisSong = new Song(songTitle, duration);
            listSong.add(addThisSong);
            System.out.println(songTitle + " has been insert in your album ");
        } else {
            System.out.println(" Song title has been already in your album ");
        }
    }

    public void removeSong(String songTitle) {
        int removeSongInt = findingSong(songTitle);
        if (removeSongInt >= 0) {
            System.out.println("Song " + songTitle + " has been removed from album");
            listSong.remove(songTitle);
        } else {
            System.out.println("Song " + songTitle + "Was not found in album");
        }
    }

    private Song findSong(String title) {
        for (Song checkSong : this.listSong) {
            if (checkSong.getSongTitle().equals(title)) {
                return checkSong;
            }
        }
        return null;
    }

    public boolean addSong(String title, Double duration) {
        if (findSong(title) == null) {
            this.listSong.add(new Song(title, duration));
            return true;
        }
        return false;
    }
}
