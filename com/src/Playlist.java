package com.songAgas;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

public class Playlist {
    public ArrayList<Album> albums = new ArrayList<>();
    private LinkedList<Song> playlist = new LinkedList<>();


    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Object item : playlist) {
            result.append(item.toString());
            result.append("\n");
        }
        return result.toString();
    }


    public void viewAlbumList() {
        LinkedList<String> tempAlbumList = new LinkedList<>();
        for (int i = 0; i < albums.size(); i++) {
            String tempAlbumName = albums.get(i).getAlbumName();
            tempAlbumList.add(tempAlbumName);
        }
        System.out.println(" you have album : " + tempAlbumList.toString());
        System.out.println();
    }


    public int findAlbum(String albumName) {
        int index = -1;
        for (int i = 0; albums.size() > i; i++) {
            if (albums.get(i).getAlbumName().equals(albumName)) {
                index = i;
                break;
            }
        }
        return index;

    }

    public void addNewAlbum(String albumName) {
        int index = findAlbum(albumName);
        if (index == -1) {
            System.out.println(albumName + " : album has been saved");
            Album newAlbum = new Album(albumName);
            albums.add(newAlbum);
        } else {
            System.out.println(" Album is already exists");
        }
    }

    public void addNewSongToAlbum(String albumName, String songName, double duration) {
        viewAlbumList();
        int albumLocation = findAlbum(albumName);
        if (findAlbum(albumName) >= 0) {
            System.out.println(" Ok, the song " + songName + " " + duration + "  has been insert to your album " + albumName);
            Album tempAlbum = albums.get(albumLocation);
            tempAlbum.addSongInit(songName, duration);

        } else {
            System.out.println(" Album is not insert to your playlist ");
        }

    }

    public void viewSonginAlbum(String albumName) {
        viewAlbumList();
        int nameInt;
        nameInt = findAlbum(albumName);
        if (nameInt >= 0) {
            String song = albums.get(nameInt).getListSong().toString();
            System.out.println("The Song in album " + albumName + " are " + song);
        } else {
            System.out.println("Album not Found");
        }
    }

    public void removeSong(String songName) {
        ListIterator<Album> listIterator = albums.listIterator();
        int removeSongInt = findSongInAlbum(songName);
        Album albumYangDiRemove = albums.get(removeSongInt);
        if (removeSongInt >= 0) {
//            albums.remove(removeSongInt);// ini lu delete semua nya
            albumYangDiRemove.removeSong(songName);
            System.out.println(" Song " + songName + " has been removed in your album");
        } else {
            System.out.println(" Song " + songName + " notfound in your album list");
        }
//        }
    }

    public void hapusSong(String songName) {
        ListIterator<Song> listIterator = playlist.listIterator();
        while (listIterator.hasNext()) {
            int removeSongInt = findSongInAlbum(songName);
            Album hapusLagu =albums.get(removeSongInt);
            if (removeSongInt >=0 ) {
                listIterator.remove();
                System.out.println(" Song " + songName + " has been removed in your album");
            } else {
                System.out.println(" Song " + songName + " notfound in your album list");
            }
            }
        }


    public int findSongInAlbum(String songName) {
        Song song = new Song(songName, null);
        int index = -1;
        for (int i = 0; albums.size() > i; i++) {
            if (song.getSongTitle().equals(songName)) {
                index = i;
                break;
            }
        }
//        for (int i = 0; albums.size() >i ; i++) {
//            if (playlist.get(i).getSongTitle().equals(songName)) {
//                index = i;
//                break;
//            }
        return index;
    }

    private int findSong(String songTitle) {
        int index = -1;
        for (int i = 0; playlist.size() > i; i++) {
            if (playlist.get(i).getSongTitle().equals(songTitle)) {
                index = i;
                // System.out.println(index + " Is current index for find Song");
            }
        }
        return index;
    }
}
