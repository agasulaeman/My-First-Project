package com.songAgas;

public class Song {
    private String songTitle;
    private Double durationSong;


    public Song(String songTitle, Double durationSong) {
        this.songTitle = songTitle;
        this.durationSong = durationSong;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public Double getDurationSong() {
        return durationSong;
    }

    public Song addSong(String songTitle,double durationSong){
        return  new Song(songTitle,durationSong);
    }

    @Override
    public String toString() {
        return "Song{" +  this.songTitle  +  " = " + this.durationSong +
                '}';
    }


}

