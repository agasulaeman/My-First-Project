public class DecimalComparator {
    public static boolean areEqualByThreeDecimalPlaces(double areEqualDecimal1, double areEqualDecimal2) {

        boolean isEqual;
        double a = Math.abs(-areEqualDecimal1);
        double b = Math.abs(areEqualDecimal2);
        a = Math.round(-areEqualDecimal1 / 1000);
        b = Math.round(areEqualDecimal2 / 1000);

        if (a == b) {
            System.out.println("The Decimal True");
            isEqual = true;
            return true;
        } else if (a > b || a < b || !(a == b)) {
            System.out.println("The Decimal Wrong");
            isEqual = false;
        }
        return false;
    }

    public static boolean sumCheck(int number1, int number2, int number3) {
        int result = number1 + number2;
        if (number3 == result) {
            System.out.printf("\nBilangan %s, ditambah %s, = %s dan dicocokan dengan %s hasil nya adalah sama",
                    number1, number2, number3, result);
            return true;
        } else {
            System.out.printf("\nBilangan %s, ditambah %s, = %s dan dicocokan dengan %s hasil nya adalah beda",
                    number1, number2, number3, result);
            return false;
        }
    }

    public static boolean checkNumber(int number1, int number2, int number3) {
        int result1 = 13;
        int result2 = 19;
        if ((number1 >= result1 && number1 <= result2) || (number2 >= result1 && number2 <= result2) || (number3 >= result1 && number3 <= result2)) {
            System.out.printf("\nAngka yang pertama adalah : %s  dan Angka yang kedua adalah : %s dan angka yang ketiga adalah : %s ",
                    number1, number2, number3);
            System.out.println("Data Valid");
            return true;
        } else {
            System.out.printf("\nAngka yang pertama adalah : %s  dan Angka yang kedua adalah : %s dan angka yang ketiga adalah : %s ",
                    number1, number2, number3, "Maka hasil nya adalah Salah Satu angka  tidak ada yg memenuhi syarat");
            System.out.println("Data Invalid");
        }
        return false;
    }

    public static boolean teenNumber(int angka1) {
        if (angka1 >= 13 && angka1 <= 19) {
            System.out.println("angka kamu adalah : " + angka1 +
                    " Angka tersebut ada di dalam range atau batas 13 - 19");
            return true;
        } else {
            System.out.println("Angka kamu adalah : " + angka1 +
                    " Angka tersebut diluar dari batas angka 13 - 19");
        }
        return false;
    }

    public static int calcFeetAndInchesToCentimeters(int ft, int inches) {
        double feetToCentimeters = 30.48 * (double) ft;
        double inchesToCentimeters = 2.54 * (double) inches;
        if ((ft >= 0) && (inches >= 0 && inches <= 12)) {
            System.out.printf("\n Your input is %s ft and %s inches =  %s cm dan %s cm",
                    ft, inches, feetToCentimeters, inchesToCentimeters);
        } else {
            System.out.printf("\n Your input is %s ft and %s inches =  %s cm dan %s cm data Invalid",
                    ft, inches, feetToCentimeters, inchesToCentimeters);
        }
        return -1;
    }

    public static int calcFeetAndInchesToCentimeters(double inches) {
        int ft = 12 * (int) inches;
        if (inches >= 0) {
            System.out.printf("\n Your input is %s inches =  %s ft",
                    inches, ft);
        } else {
            System.out.printf("\n Your input is %s inches =  %s ft data invalid",
                    inches, ft);
        }
        return -1;
    }

    public static void printYearandDays(long minutes) {
        if (minutes <= 0) {
            System.out.println("Invalid Value");
        }

        int days = (((int) minutes / 60) / 24) % 365;
        int years = ((int) minutes / 60 / 24) / 365;
        System.out.printf("Your input Minutes is %s  = %s year %s days\n"
                , minutes, years, days);
    }

    public static int checkEqualNumber(int number1, int number2, int number3) {
        if ((number1 <= 0) || (number2 <= 0) || (number3 <= 0)) {
            System.out.print("\n invalid number , because the number is 0 or minus");
        } else if ((number1 != number2) && (number1 != number3) && (number2 != 3)) {
            System.out.println("\n The three number all different");
        } else if ((number1 == number2) && (number1 == number3) && (number2 == number3)) {
            System.out.println(" The Three Number are valid or Equals");
        }
        return 1;
    }

    public static boolean isCatPlaying(boolean isCatMain, int temperature){
        if ((temperature >= 25) && (temperature <=35)) {
            isCatMain = true;
            System.out.println("Ayo bermain bersama kucing mu karena temperature di luar adalah " +temperature);
            return  isCatMain;
        }else{
            isCatMain = false;
            System.out.println("Kucing mu tidak mau main karena temperature diluar adalah "+temperature);
            return isCatMain;
        }
    }
}

