public class AreaCalculator {

    public static String errorMessage = " your data is Invalid Value";

    public static double areaCircle(double radius) {
        if ((radius <= 0.0) || (radius < 0)) {

            System.out.println(errorMessage);
            return -1.0;
        }
        double phi = Math.PI;
        double circleArea = radius * radius * phi;
        System.out.printf("\n Your input radius is %s dengan hasil perhitungan menghasilkan %s \n", radius, circleArea);
        return circleArea;
    }
    public static double areaCircle(double radius,double area2) {
        if ((radius <= 0.0) || (area2 <= 0.0)) {

            System.out.println(errorMessage);
            return -1.0;
        }
        double areaRectangle = radius * area2;
        //double circleArea = radius * radius * phi;
        System.out.printf("\n Your input radius is %s dan area2 nya adalah %s dengan hasil perhitungan menghasilkan %s \n", radius, area2,areaRectangle);
        return areaRectangle;
    }

}
