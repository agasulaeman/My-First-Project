package gangzarseptember.modifier;

public class Barang {
    private String namaBarang;
    private int hargaBarang;

    public Barang(String namaBarang, int hargaBarang) {
        this.namaBarang = namaBarang;
        this.hargaBarang = hargaBarang;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public int getHargaBarang() {
        return hargaBarang;
    }

    public void menampilkanHargadanNamaBarang(){
        String namaBarang = "Telor";
        int hargaBarang =15000;
        System.out.println(namaBarang + " "+hargaBarang);
    }

    public String tampilHargaBarangdanNamaBarang(){
        menampilkanHargadanNamaBarang();
        return null;
    }
}
