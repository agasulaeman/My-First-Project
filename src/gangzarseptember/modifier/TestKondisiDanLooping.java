package gangzarseptember.modifier;

public class TestKondisiDanLooping {
    private String namaPeserta = "Gangzar";


    public TestKondisiDanLooping setNamaPeserta(String namaPeserta) {
        this.namaPeserta = namaPeserta;
        return this;
    }

    public void menggunakanInfinityLoop() {
        for (int i = 1; i <= 5; i++) {
            System.out.println(namaPeserta + " " + i);
        }
    }

    public void menggunakanKondisiIf(String namaPeserta) {
        if (this.namaPeserta.equals("Gangzar 1")) {
            System.out.println(" Nama peserta itu bukan Gangzar 1");
        } else if ((this.namaPeserta.equals("Gangzar 2")) || !this.namaPeserta.equals("Gangzar 3")) {
            System.out.println(" tidak ada nama Gangzar 2, tetapi ada yang bernama " + namaPeserta);
        } else {
            System.out.println("Tidak ada di kelas yang bernama Gangzar");
        }
    }

}
