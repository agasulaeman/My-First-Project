package gangzarseptember.oop.inheritance;

public class SepedaGunung extends Sepeda{
    public int suspensiSepeda;

    public SepedaGunung(int gearSepeda, int speedSepeda,int suspensiAwal) {
        super(gearSepeda, speedSepeda);
        suspensiSepeda = suspensiAwal;
    }

   public void setSuspensiSepeda (int suspensiBaru){
        suspensiSepeda = suspensiBaru;
   }

    @Override
    public String toString() {
        return (super.toString() + " suspensi sepeda kamu adalah " +suspensiSepeda);

        /*"SepedaGunung{" +
                "suspensiSepeda=" + suspensiSepeda +
                '}';*/
    }
}
