package gangzarseptember.oop.inheritance;

public class Sepeda {
    public int gearSepeda;
    public int speedSepeda;

    public Sepeda(int gearSepeda, int speedSepeda) {
        this.gearSepeda = gearSepeda;
        this.speedSepeda = speedSepeda;
    }

    public void sepedaMelakukanRem(int kecepatanSaatRem){
            speedSepeda -= kecepatanSaatRem;
        System.out.println(" kecepatan sepeda kamu saat melakukan Rem " +speedSepeda);
    }

    public void sepedaSaatmelakukanAkselerasi (int kecepatanNgebut){
        speedSepeda +=kecepatanNgebut;
        System.out.println(" kecepatan sepeda kamu saat melakukan akselerasi atau ngebut " +speedSepeda);
    }

    @Override
    public String toString() {
        return "Sepeda{" +
                "gearSepeda=" + gearSepeda +
                ", speedSepeda=" + speedSepeda +
                '}';
    }
}
