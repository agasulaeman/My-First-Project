package gangzarseptember.oop;

import gangzarseptember.oop.encapsulation.Karyawan;
import gangzarseptember.oop.inheritance.SepedaGunung;
import gangzarseptember.oop.polymorphism.Mobil;

public class TestOop {
    public static void main(String[] args) {
        Mobil mobil = new Mobil(4, null, null, null, true);
        mobil.kendaraanBergerak(4, "Honda", "Honda jazz", "City car", true);

        mobil.kendaraanStop(2, "Harley Davidson", "Moge", "motor", false);
        mobil.rodaHarlevDavidson(2);

        SepedaGunung sg = new SepedaGunung(1, 20, 15);
        sg.sepedaMelakukanRem(5);

        sg.sepedaSaatmelakukanAkselerasi(40);

        Karyawan karyawan = new Karyawan();
        karyawan.setNamaKaryawan("Gangzar");
        karyawan.setNomorIndukKaryawan(45421);
        karyawan.setJabatan("IT MIS");

        System.out.println("\n Nama Karyawan " + karyawan.getNamaKaryawan() + " \n" +
                "Dengan Id Karyawan : " + karyawan.getNomorIndukKaryawan() + " \n " +
                "Jabatan Saat ini : " + karyawan.getJabatan());
    }
}
