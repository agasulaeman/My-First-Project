package gangzarseptember.oop.polymorphism;

public class Mobil extends Kendaraan {
    private int jumlahBan;
    private String namaMerkKendaraan;
    private String tipeKendaraan;

    public Mobil(int jumlahBan, String namaMerkKendaraan, String jenisKendaraan, String tipeKendaraan, boolean isBermesin) {
        super(jenisKendaraan, isBermesin);
        this.jumlahBan = jumlahBan;
        this.namaMerkKendaraan = namaMerkKendaraan;
        this.tipeKendaraan = tipeKendaraan;

    }

    public void kendaraanBergerak(int jumlahBan, String namaMerkKendaraan, String jenisKendaraan, String tipeKendaraan, boolean isBermesin) {
        System.out.println(" memanggil fungsi kendaraan bergerak ");
        if (isBermesin()) {
            System.out.println("Kendaraan anda bergerak kendaraan anda adalah kendaraan bermotor \n");

        } else {
            System.out.println(" kendaraan anda sepeda atau skateboard");
        }
    }

    public void kendaraanStop(int jumlahBan, String namaMerkKendaraan, String jenisKendaraan, String tipeKendaraan, boolean isBermesin) {
        System.out.println(" memanggil fungsi kendaraan Stop ");
        if (isBermesin()) {
            System.out.println("Kendaraan anda berhenti , kendaraan anda habis bensin ");

        } else {
            System.out.println(" kendaraan anda sedang ada masalah mesin ");
        }
    }


    public int rodaHarlevDavidson(int jumlahBan) {

        int evolusiJumlahBan = 1;
        int hasilevolusiJumlahBan = jumlahBan + evolusiJumlahBan;
        System.out.println("kendaraan bermotor evolusi menjadi beroda " + hasilevolusiJumlahBan);
        return hasilevolusiJumlahBan;
    }
}
