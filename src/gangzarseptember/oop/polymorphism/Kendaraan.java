package gangzarseptember.oop.polymorphism;

public class Kendaraan {
    private String jenisKendaraan;
    private boolean isBermesin;

    public Kendaraan(String jenisKendaraan, boolean isBermesin) {
        this.jenisKendaraan = jenisKendaraan;
        this.isBermesin = isBermesin;
    }

    public Kendaraan setJenisKendaraan(String jenisKendaraan) {
        this.jenisKendaraan = jenisKendaraan;
        return this;
    }

    public Kendaraan setBermesin(boolean bermesin) {
        isBermesin = bermesin;
        return this;
    }

    public void apakahMemakaiBensin(){
        if (isBermesin = false){
            System.out.println("Kendaraan ini tidak menggunkan bahan bakar");
        }else{
            System.out.println("kendaraan ini memakai bahan bakar");
        }

    }
    public boolean isBermesin(){
        return isBermesin;
    }
}
