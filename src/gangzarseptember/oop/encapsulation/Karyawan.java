package gangzarseptember.oop.encapsulation;

public class Karyawan {
    private String namaKaryawan;
    private int nomorIndukKaryawan;
    private String jabatan;

    public String getNamaKaryawan() {
        return namaKaryawan;
    }

    public Karyawan setNamaKaryawan(String namaKaryawan) {
        this.namaKaryawan = namaKaryawan;
        return this;
    }

    public int getNomorIndukKaryawan() {
        return nomorIndukKaryawan;
    }

    public Karyawan setNomorIndukKaryawan(int nomorIndukKaryawan) {
        this.nomorIndukKaryawan = nomorIndukKaryawan;
        return this;
    }

    public String getJabatan() {
        return jabatan;
    }

    public Karyawan setJabatan(String jabatan) {
        this.jabatan = jabatan;
        return this;
    }
}
