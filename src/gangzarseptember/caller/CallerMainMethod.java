package gangzarseptember.caller;

import gangzarseptember.modifier.Barang;
import gangzarseptember.modifier.TestKondisiDanLooping;

public class CallerMainMethod {
    public static void main(String[] args) {
        Barang barang = new Barang(null, 0);
        barang.tampilHargaBarangdanNamaBarang();

        TestKondisiDanLooping testKondisiDanLooping = new TestKondisiDanLooping();
        testKondisiDanLooping.menggunakanKondisiIf("Agas");
        testKondisiDanLooping.menggunakanInfinityLoop();
    }
}
